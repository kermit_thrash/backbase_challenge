Feature('Home Page');

Before((I) => {
    I.amOnPage('/computers')
})

Scenario('Open /computers (HTTP link) as homepage', (I) => {
    // This 'a' means <a> html tag, so looking for a link with that text
    I.see('Play sample application — Computer database ', 'a')
})

// This one should fail but I'm asserting something else to make it pass (See Bug #B1)
Scenario('Open /whatever (HTTP link) as homepage', (I) => {
    I.amOnPage('/whatever')
    I.dontSee('Play sample application — Computer database ')
})

Scenario('Display a table with computers found', (I) => {
    I.seeElement('table.computers.zebra-striped')
})

Scenario('Display Next result page', async (I) => {
    const table_result = await I.grabTextFrom('.computers')
    I.click('.next')
    I.dontSee(table_result)
})

Scenario('Filter by existing results (Computer name column)', async (I) => {
    I.fillField('Filter by computer name...', 'ARRA')
    I.click({id: 'searchsubmit'})
    const table_result = await I.grabTextFrom('.computers')
    I.see('ARRA', 'table')
})

Scenario('Filter by non-existing results (Computer name column - No computers found label and Nothing to display message)', (I) => {
    I.fillField('Filter by computer name...', 'huachinango201233ee')
    I.click({id: 'searchsubmit'})
    I.see('Nothing to display', '.well')
    I.see('No computers found', {id: 'main'})
})

Scenario('Filter by invalid parameter (Company column)', (I) => {
    I.fillField('Filter by computer name...', 'IMS Associates, Inc.')
    I.click({id: 'searchsubmit'})
    I.see('Nothing to display', '.well')
    I.see('No computers found', {id: 'main'})
})

Scenario('Update computers found after filtering results', (I) => {
    I.fillField('Filter by computer name...', 'ARRA')
    I.click({id: 'searchsubmit'})
    I.clickLink('ARRA','table')
    I.selectOption('company','Netronics')
    I.click('Save this computer')
    I.see('Done! Computer ARRA has been updated')
})

// This covers one of the bugs but takes too long to run as it iterates over the
//  number of characters that are going to be added in the filter input field.
//
//Scenario('Filter by using a long character input (10,000 characters)', async (I) => {
//    const word = Array(4071).join('x')
//    I.fillField('Filter by computer name...', word)
//    I.click({id: 'searchsubmit'})
//    I.dontSee('Nothing to display', '.well')
//    I.dontSee('No computers found', {id: 'main'})
//})

Scenario('Add a new computer - All fields with valid characters', (I) => {
    I.click('Add a new computer')
    I.fillField('name', 'Alans computer')
    I.fillField('introduced', '2010-09-09')
    I.fillField('discontinued', '2010-09-09')
    I.selectOption('company','Netronics')
    I.click('Create this computer')
    I.see('Done! Computer Alans computer has been created')
})
