exports.config = {
  tests: '../scenarios/*_test.js',
  output: '../../_output',
  mocha: {
    reporterOptions: {
      reportDir: "../../_output"
    }
  },
  helpers: {
    Puppeteer: {
      url: 'https://computer-database.herokuapp.com',
      show: false,
      waitForAction: 200
    },
  },
  include: {
    I: '../steps/steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'Backbase'
};
