# Requirements
NodeJS v 8.9 or higher required

# Install

Run

	npm install


# Running tests

Acceptance:

	npm test

Acceptance tests are ran by Puppeeter, you might want to display the UI of the app under test, this could be done by running the next command:

	npm test -- --override '{ "helpers": {"Puppeteer": {"show": "true"}}}'

# Reports

Append to any of the commands described above the next:

	-- --reporter mochawesome
Example:

	npm test -- --reporter mochawesome

 This will generate a folder named **_output** in root, you'll be able to find a really cute report done by mocha-awesome library.

# QA comments & Documentation

I didn't add all the test cases because I created 89 test scenarios, so it would keep me busy for a long time, which sadly I can't manage that right now.

Test Scenarios, (Manual) Test Execution and bugs can be found in here:

https://www.evernote.com/shard/s102/sh/dbf7081d-60e8-4312-9d5f-7bdf22f1afbd/399b1dfc191404a2965a71ecef239a0b

I used CodeceptJs to automate, didn't create page objects or dynamic data interaction because that would take me more time. The tests automated are the same as the ones described in the test execution.
